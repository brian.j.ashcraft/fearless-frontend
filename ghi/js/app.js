function createCard(name, description, pictureUrl, formatStart, formatEnd, location) {
    return `
    <div classname="container">
      <div class="card">
        <div class="shadow">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h4 class="card-title">${name}</h4>
            <h6 class="card-subtitle text-muted mb-2">${location}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${formatStart} - ${formatEnd}</div>
      </div>
    </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details);
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date((details.conference.starts))
            const formatStart = `${startDate.getMonth()}/${startDate.getDate()}/${startDate.getFullYear()}`
            const endDate = new Date((details.conference.ends))
            const formatEnd = `${endDate.getMonth()}/${endDate.getDate()}/${endDate.getFullYear()}`
            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, formatStart, formatEnd, location);
            const column = document.querySelector('.row');

            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
        console.error(e);
        // Figure out what to do if an error is raised
    }

  });
